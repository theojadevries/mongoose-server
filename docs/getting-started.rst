======================================
 Getting Started with mongoose-server
======================================

Install mongoose-server
=======================

To get started, you need to install mongoose-server. An installer is currently
not available. You should build the application yourself from source, see below.

However, for `Archlinux`_ users, there is a package in the `AUR`_.

.. _`Archlinux`: https://www.archlinux.org
.. _`AUR`: https://aur.archlinux.org/packages/mongoose-server

Build mongoose-server from source
=================================

#. Download the `latest stable source`_
#. Unpack, e.g. using 7-zip
#. Create a subdirectory build
#. cd build
#. cmake -DMONGOOSE-SERVER_EMBED_MONGOOSE=ON -DMONGOOSE-SERVER_STATIC_LINK_LIBCONFIG=ON ../
#. make
#. check the conf file and make modifications if needed
#. fire it up in a console and check localhost

.. _`latest stable source`: https://bitbucket.org/theojadevries/mongoose-server/downloads/mongoose-server-1.0.tar.gz
