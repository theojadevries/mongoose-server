.. mongoose-server documentation master file, created by
   sphinx-quickstart on Sun Jan 10 20:43:17 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================
 mongoose-server
=================

**mongoose-server** is an easily configurable cross-platform http server. It is based on `mongoose`_
and `libconfig`_. Its aim is to make it simple to get your localhost up and running.

.. _`mongoose`: https://github.com/cesanta/mongoose
.. _`libconfig`: https://github.com/hyperrealm/libconfig

License
=======

**mongoose-server** is made available under a GPLv2 license.

.. toctree::

   getting-started
