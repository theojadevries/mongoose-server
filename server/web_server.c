/*
 * mongoose-server is an easily configurable cross-platform http server.
 * Copyright (c) 2015 Theo J.A. de Vries

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#define MGS_VERSION "1.0"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libconfig.h"
#include "mongoose.h"

static struct mg_serve_http_opts s_http_server_opts;
static int s_sig_received = 0;

static void ev_handler(struct mg_connection *nc, int ev, void *p) {
    if (ev == MG_EV_HTTP_REQUEST) {
        mg_serve_http(nc, (struct http_message *)p, s_http_server_opts);
    }
}

static void signal_handler(int sig_num) {
    signal(sig_num, signal_handler);
    s_sig_received = sig_num;
}


int main(int argc, char **argv) {

    char *fname = NULL;

    /* load the file location from command line or use the default*/
    if (argc == 2) // one argument was passed
    {
        fname = (char *)malloc (strlen (argv[1]) + 1);
        if (fname == NULL) return(EXIT_FAILURE);          // No memory
        strcpy (fname,argv[1]);
    } else {
        fname = (char *)malloc (strlen ("mongoose-server.conf") + 1);
        if (fname == NULL) return(EXIT_FAILURE);          // No memory
        strcpy (fname,"mongoose-server.conf");
    }
    printf("Trying to read config file: %s... ", fname);

    config_t cfg;
    config_init(&cfg);

    /* try to read the config from file */
    struct stat buffer;
    if (stat (fname, &buffer) == 0) {
        // file exists
        if(! config_read_file(&cfg, fname))
        {
            printf("errors encountered.\n");
            fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
                    config_error_line(&cfg), config_error_text(&cfg));
            config_destroy(&cfg);
            free(fname);
            return(EXIT_FAILURE);
        } else {
            printf("done.\n");
        }
    } else {
        printf("\nConfig file does not exist or is unreadable; using defaults.");
    }
    free(fname);

    struct mg_mgr mgr;
    mg_mgr_init(&mgr, NULL);

    /* Set up the connection */
    const char *s_http_port = "8080";
    if(config_lookup_string(&cfg, "http_port", &s_http_port)) {
        printf("http_port: %s\n", s_http_port);
    }

    struct mg_connection *nc;
    nc = mg_bind(&mgr, s_http_port, ev_handler);
    if (nc == NULL) {
        fprintf(stderr, "Cannot bind to port %s\n", s_http_port);
        mg_mgr_free(&mgr);
        config_destroy(&cfg);
        return(EXIT_FAILURE);
    }


    /* Set up HTTP server parameters */
    mg_set_protocol_http_websocket(nc);

    const char *s_document_root = "/srv/http";
    if(config_lookup_string(&cfg, "document_root", &s_document_root)) {
        printf("document_root: %s\n", s_document_root);
    }
    s_http_server_opts.document_root = s_document_root;

    const char *s_index_files = "index.html,index.htm,index.php";
    if(config_lookup_string(&cfg, "index_files", &s_index_files)) {
        printf("index_files: %s\n", s_index_files);
    }
    s_http_server_opts.index_files = s_index_files;

    const char *s_auth_domain = "localhost";
    if(config_lookup_string(&cfg, "auth_domain", &s_auth_domain)) {
        printf("auth_domain: %s\n", s_auth_domain);
    }
    s_http_server_opts.auth_domain = s_auth_domain;

    const char *s_enable_directory_listing = "yes";
    if(config_lookup_string(&cfg, "enable_directory_listing", &s_enable_directory_listing)) {
        printf("enable_directory_listing: %s\n", s_enable_directory_listing);
    }
    s_http_server_opts.enable_directory_listing = s_enable_directory_listing;

    const char *s_ip_acl = "-0.0.0.0/0,+127.0.0.1";
    if(config_lookup_string(&cfg, "ip_acl", &s_ip_acl)) {
        printf("ip_acl: %s\n", s_ip_acl);
    }
    if ( mg_check_ip_acl(s_ip_acl, 0) < 0 ) {
        printf("Malformed ACL!\n");
    } else {
        s_http_server_opts.ip_acl = s_ip_acl;
    }

    /* Start a polling loop that bails out when a signal arrives */
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    printf("\nStarting web server on port %s\n", s_http_port);
    while (!s_sig_received) {
        mg_mgr_poll(&mgr, 1000);
    }

    printf("\nExiting web server on signal %d\n", s_sig_received);

    mg_mgr_free(&mgr);
    config_destroy(&cfg);
    return(EXIT_SUCCESS);
}
