cmake_minimum_required(VERSION 2.8)

if(Do_SuperBuild)

  # build prerequisites...

  project(mongoose-server-external)

  #now build this project
  include(ExternalProject)
  ExternalProjectAdd(
    mongoose-server
    DEPENDS ${SERVER_DEPENDENCIES}
    SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}
    CMAKE_ARGS -DDo_SuperBuild:BOOL=OFF
  )
else(Do_SuperBuild)

  #
  # all the CMake stuff to build your program

  aux_source_directory(. SRC_LIST)
  aux_source_directory(../docs DOCS_SRC_LIST)
  configure_file(../conf/mongoose-server.conf . )

  include_directories( ${SERVER_INCLUDE_DIR} )
  link_directories( ${SERVER_LINK_DIR} )

  if( MONGOOSE-SERVER_EMBED_MONGOOSE )
    list(APPEND MONGOOSE_SRC_LIST "${CMAKE_BINARY_DIR}/mongoose/src/mongoose/mongoose.c" )
    set_source_files_properties(
      ${MONGOOSE_SRC_LIST}
      PROPERTIES GENERATED TRUE)
    add_definitions( -DMONGOOSE_ENABLE_THREADS )
    find_package( Threads REQUIRED )
  endif( MONGOOSE-SERVER_EMBED_MONGOOSE )

  list(APPEND SRC_LIST ${MONGOOSE_SRC_LIST} )
  # message("SRC_LIST is ${SRC_LIST}")

  add_executable(${PROJECT_NAME}
    ${SRC_LIST}
  )

  list(APPEND SERVER_LIBS config ${CMAKE_THREAD_LIBS_INIT})

  target_link_libraries(${PROJECT_NAME}
    ${SERVER_LIBS}
  )
  if( NOT MONGOOSE-SERVER_EMBED_MONGOOSE )
    message (STATUS "Linking mongoose from system...")
    link_directories( ${CMAKE_BINARY_DIR}/lib )
  endif( NOT MONGOOSE-SERVER_EMBED_MONGOOSE )

  if(WIN32)
    target_link_libraries(
      ${PROJECT_NAME}
      ws2_32
    )
  endif()

  install(TARGETS
    ${PROJECT_NAME}
   RUNTIME DESTINATION bin
  )

endif(Do_SuperBuild)


