# mongoose-server #

**mongoose-server** is an easily configurable cross-platform http server. It is
based on [mongoose](https://github.com/cesanta/mongoose) and
[libconfig](https://github.com/hyperrealm/libconfig). Its aim is to make it
simple to get your localhost up and running.

Contributing
------------

Submit wishes, comments, patches, etc. via
[Bitbucket](https://bitbucket.org/theojadevries/mongoose-server).

License
-------

**mongoose-server** is made available under a GPL license, version 2.0;
see LICENSE for details.
